# Veille2toot

Un dépôt pour automatiser la publication d’un flux Web vers Mastodon.

Ce dépôt utilise [feediverse](https://github.com/edsu/feediverse).

## Comment l’utiliser ?

### Sur votre compte Mastodon

Commencez par créer une « application ».
Pour cela, rendez-vous dans « Préférences > Développement > Nouvelle application ».
Donnez un nom (par exemple « Flus » si vous automatisez la publication de votre veille [Flus](https://flus.fr)) et une URL (ex. `https://flus.fr`).
Ne touchez pas à l’URL de redirection.
Décochez les cases pré-cochées, et cochez seulement « `write:statuses` (publier des messages) ».

Cliquez ensuite sur le nom de l’application créée : vous aurez besoin des trois informations qui s’affichent ici (« ID de l’application », « Secret » et « Votre jeton d’accès »).
**Attention, ces données sont sensibles, ne les partagez avec personne !**

### Sur Framagit

Clonez ce dépôt.

Dans votre dépôt cloné, sous « Paramètres > Intégration et livraison continue », ouvrez le menu « Variables ».

Vous aurez besoin d’ajouter cinq variables :

- clé `MASTO_CLIENT_ID` : copiez-y la valeur « ID de l’application » de Mastodon (cochez « Masquer la variable » et décochez « Développer la référence de la variable »)
- clé `MASTO_CLIENT_SECRET` : copiez-y la valeur « Secret » de Mastodon (cochez « Masquer la variable » et décochez « Développer la référence de la variable »)
- clé `MASTO_ACCESS_TOKEN` : copiez-y la valeur « Votre jeton d’accès » de Mastodon (cochez « Masquer la variable » et décochez « Développer la référence de la variable »)
- clé `MASTO_URL` : copiez-y l’URL de votre Mastodon, par exemple `https://framapiaf.org` (décochez « Développer la référence de la variable »)
- clé `FEED_URL` : copiez-y l’URL du flux Web que vous souhaitez publier (décochez « Développer la référence de la variable »)

Dans « Intégration et livraison continue > Planifications », créez une nouvelle planification.
Donnez une description (ce que vous voulez), puis choisissez le modèle d’intervalle « Personnalisé » avec la valeur `0 * * * *` (signifiant « toutes les heures »).
Choisissez votre fuseau horaire (ex. « Paris »), puis validez.
Enfin, cliquez sur le bouton « Lancer » de la planification (le bouton en forme de triangle).

Désormais, feediverse va être appelé toutes les heures pour publier les nouvelles publications de votre flux Web.
Attention : il ne prendra en compte que les éléments publiés **après** la première exécution !

### Éditez le format de message

Dans le fichier `feediverse.sample.conf`, vous pouvez modifier la valeur de l’élément `template` pour l’adapter à votre besoin.
**Ne touchez surtout pas au reste !**

Vous pourriez avoir besoin de [la documentation de feediverse](https://github.com/edsu/feediverse#post-format) pour modifier le format du message.
